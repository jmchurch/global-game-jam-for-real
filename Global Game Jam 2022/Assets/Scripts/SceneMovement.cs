using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMovement : MonoBehaviour
{
    public void MoveToMainLevel()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void MoveToTitleScreen()
    {
        SceneManager.LoadScene("Title");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        SceneManager.LoadScene("End");
    }
}
