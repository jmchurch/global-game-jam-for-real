using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class CameraMovementToPoints : MonoBehaviour
{
    public List<Transform> camPositions = new List<Transform>();
    public List<TextMeshProUGUI> tutorialTexts = new List<TextMeshProUGUI>();
    private static List<Transform> cameraPositions = new List<Transform>();
    private static List<TextMeshProUGUI> tutorialMessages = new List<TextMeshProUGUI>();
    private static GameObject cam;

    private void Awake()
    {
        cameraPositions = camPositions;
        tutorialMessages = tutorialTexts;
        cam = GameObject.Find("Main Camera");
    }

    private void Update()
    {
        //Debug.Log(cam);
    }

    public static void MovePointOne()
    {
        cam.transform.DOMove(cameraPositions[0].position, 3);
        tutorialMessages[0].gameObject.SetActive(false);
        tutorialMessages[1].gameObject.SetActive(true);
    }

    public static void MovePointTwo()
    {
        cam.transform.DOMove(cameraPositions[1].position, 3);
        tutorialMessages[1].gameObject.SetActive(false);
        tutorialMessages[2].gameObject.SetActive(true);
    }

    public static void MovePointThree()
    {
        cam.transform.DOMove(cameraPositions[2].position, 3);
        tutorialMessages[2].gameObject.SetActive(false);
        tutorialMessages[3].gameObject.SetActive(true);
    }
}
