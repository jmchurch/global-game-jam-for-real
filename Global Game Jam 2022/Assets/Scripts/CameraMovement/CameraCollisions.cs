using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollisions : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (gameObject.name == "TransitionOne")
        {
            CameraMovementToPoints.MovePointOne();
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
        
        if (gameObject.name == "TransitionTwo")
        {
            CameraMovementToPoints.MovePointTwo();
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
        
        if (gameObject.name == "TransitionThree")
        {
            CameraMovementToPoints.MovePointThree();
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
        
    }
}
