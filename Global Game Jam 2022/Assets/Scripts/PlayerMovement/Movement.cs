using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Rigidbody2D rb;
    public Transform groundCheck;
    public Animator animator;

    public GameObject PostProcessVolume;
    public ParticleSystem JumpEffect;
    public AudioSource GravSwitchAudio;
    public AudioSource CityMusic;
    
    [SerializeField] private float jumpPower;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float maxMoveSpeed;
    [SerializeField] private LayerMask whatIsGround;

    private SpriteRenderer _spriteRenderer;
    private bool _grounded;
    private float _groundedRadius = .2f;
    private bool _jumpCharging = false;
    private bool _switchAvailable = true;
    private bool _onCeiling = false;
    private bool idle;
    private bool move;
    private bool moving;
    private bool falling;

    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_onCeiling)
        {
            CityMusic.pitch = -.5f;
        }
        else
        {
            CityMusic.pitch = 1;
        }
        _grounded = false;
        
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, _groundedRadius, whatIsGround);
        if (colliders.Length > 0)
        {
            // reset the gravity multiplier that was only for the fall
            if (falling)
                rb.gravityScale /= 4;
            _grounded = true;
            falling = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (rb.velocity.x > -maxMoveSpeed)
                rb.AddForce(new Vector2(-moveSpeed, 0));

            if (!_onCeiling)
                _spriteRenderer.flipX = true;
            else
                _spriteRenderer.flipX = false;
        }

        if (Input.GetKey(KeyCode.D))
        {
            if (rb.velocity.x < maxMoveSpeed)
                rb.AddForce(new Vector2(moveSpeed, 0));

            if (!_onCeiling)
                _spriteRenderer.flipX = false;
            else
                _spriteRenderer.flipX = true;
        }

        if (Math.Abs(rb.velocity.x) < 1)
        {
            moving = false;
            idle = true;
        }
        else
        {
            moving = true;
            idle = false;
        }

        if (Input.GetKey(KeyCode.W)) {
            if (_grounded)
            {
                if (!_jumpCharging)
                {
                    Debug.Log("Started Charging Jump");
                    idle = false;
                    _jumpCharging = true;
                    StartCoroutine(nameof(ChargeJump));
                }
            }
        }

        if (Input.GetKey(KeyCode.Space) && _switchAvailable && _grounded)
        {
            StartCoroutine(nameof(SwitchCharge));
            rb.gravityScale *= -4;
            falling = true;
            Debug.Log(rb.gravityScale);
            if (rb.gravityScale > 0)
            {
                gameObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
                _onCeiling = false;
            }
            else
            {
                gameObject.transform.eulerAngles = new Vector3(0f, 0f, 180f);
                _onCeiling = true;
            }
            PostProcessVolume.SetActive(!PostProcessVolume.activeSelf);
            GravSwitchAudio.timeSamples = 50000; // skips the first few moments of the audio clip so it plays immediately
            GravSwitchAudio.Play();
        }

        animator.SetBool("Idle", idle);
        animator.SetBool("Move", moving);
        animator.SetBool("Jump", !_grounded);
        animator.SetBool("Charging", _jumpCharging);
        animator.SetBool("Falling", falling);
    }

    private IEnumerator ChargeJump()
    {
        yield return new WaitForSeconds(.2f);
        if (jumpPower < 700)
        {
            jumpPower += 20;
        }

        Debug.Log("curr jump power "+ jumpPower);
        
        if (!Input.GetKey(KeyCode.W) && _grounded)
        {
            Debug.Log("let go of W");
            _grounded = false;
            float updatedJumpPower;
            if (_onCeiling)
            {
                updatedJumpPower = jumpPower * -1;
            }
            else
            {
                updatedJumpPower = jumpPower;
            }
            rb.AddForce(new Vector2(0f, updatedJumpPower));
            
            JumpEffect.transform.position = transform.position;
            JumpEffect.Play();

            jumpPower = 500;
            _jumpCharging = false;
            yield break;
        }
        StartCoroutine(nameof(ChargeJump));
    }

    private IEnumerator SwitchCharge()
    {
        _switchAvailable = false;
        yield return new WaitForSeconds(.05f);
        _switchAvailable = true;
    }
}
